package group15.explodingkittens.Model.Card;

public enum CardCategory {
    EXPLODING_KITTEN,
    DEFUSE,
    ATTACK,
    SEE_THE_FUTURE,
    SKIP,
    NOPE,
    FAVOR,
    SHUFFLE,
    TACO_CAT,
    BEARD_CAT,
    CATTER_MELON,
    HAIRY_POTATO_CAT,
    RAINBOW_RALPHING_CAT
}
