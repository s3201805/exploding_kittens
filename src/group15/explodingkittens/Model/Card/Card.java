package group15.explodingkittens.Model.Card;

/**
 * Class for creating a Card.
 * A card has a type which is represented by an enum.
 */
public class Card {
    private CardCategory cardType;

    /**
     * Constructor for card, takes the card name as a parameter
     * @param cardType
     */
    public Card(CardCategory cardType) {
        this.cardType = cardType;
    }

    public CardCategory getCardType(){
        return cardType;
    }
}