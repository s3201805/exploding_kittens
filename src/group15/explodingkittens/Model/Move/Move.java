package group15.explodingkittens.Model.Move;

import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Player.Player;

import java.util.ArrayList;

/**
 * Class for a specific move, mostly used for the moveType enum and creating a new type.
 */
public class Move {
    private MoveType moveType;
    private Card card;
    private ArrayList<Card> cards;
    private Player targetPlayer;

    /**
     * Draw move
     * @param moveType
     * @param player
     */
    public Move(MoveType moveType, Player player) {
        this.moveType = moveType;
        this.targetPlayer = player;
    }

    /**
     * Play card normal move
     * @param moveType
     * @param card
     * @param player
     */
    public Move(MoveType moveType, Card card, Player player) {
        this.moveType = moveType;
        this.card = card;
        this.targetPlayer = player;
    }

    /**
     * Play 2/3 of the same move
     */
    public Move(MoveType moveType, ArrayList<Card> cards, Player player) {
        this.moveType = moveType;
        this.cards = cards;
        this.targetPlayer = player;
    }

    public MoveType getMoveType() {
        return moveType;
    }


    public Card getCard() {
        return card;
    }


    public ArrayList<Card> getCards() {
        return cards;
    }
}
