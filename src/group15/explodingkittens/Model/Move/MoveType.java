package group15.explodingkittens.Model.Move;

/**
 * Enum for the types of move
 */
public enum MoveType {
    PLAY,
    DRAW,
    TWO,
    THREE,
}
