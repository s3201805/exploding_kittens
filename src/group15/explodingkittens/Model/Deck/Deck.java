package group15.explodingkittens.Model.Deck;

import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.Player;
import group15.explodingkittens.Model.Player.HumanPlayer;
import group15.explodingkittens.Model.Player.ComputerPlayer;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class for the deck of cards on the table.
 */
public class Deck {

    /**
     * Size of the deck. When a card is drawn, the size decreases by one.
      */
    private int size;

    /**
     * All the cards in the deck, if drawn, card is removed from the list
     */
    private ArrayList<Card> cardsDeck;
    /**
     * Number of players in the game.
     */
    private int numberOfPlayers;

    private CardCategory drawnCard;
    /**
     * Creates a new deck with numberOfPlayers as an amount of players.
     * Sets the size to the total deck - players
     * @param numberOfPlayers
     */
    public Deck(int numberOfPlayers) {
        this.cardsDeck = createDeck(numberOfPlayers);
        this.numberOfPlayers = numberOfPlayers;
        size = 56 - (numberOfPlayers * 8);
        shuffle();
    }

    /**
     * Shuffles the deck
     */
    public void shuffle(){
        try {
            Collections.shuffle(cardsDeck);
            System.out.println("The deck is shuffled");
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Distributes the initial shuffled deck of cards, every player gets 8 cards.
     * Gives 7 cards to each player -> gives each player a defuse card -> adds exploding kittens according to players
     * @ensures player.getCardsInHand.getSize() = 8
     * @ensures player.hasThisCard(Card.Type.DEFUSE)
     * @ensures !player.hasThisCard(Card.Type.EXPLODING_KITTEN)
     * @param players
     */
    public void distribute(ArrayList<Player> players){
        for (Player player : players) {
            ArrayList<Card> playerCards = new ArrayList<>();

            for (int i = 0; i < 7; i++) {
                playerCards.add(cardsDeck.remove(0));
            }

            player.setCardsInHand(playerCards);
            player.addToHand(new Card(CardCategory.DEFUSE));
        }

        int numberOfExplodingKittens = calculateExplodingKittens(numberOfPlayers);
        for (int i = 0; i < numberOfExplodingKittens; i++) {
            cardsDeck.add(new Card(CardCategory.EXPLODING_KITTEN));
        }
    }

    /**
     * Creates players for the game.
     * @param numberOfHumanPlayers
     * @param numberOfCPU
     * @param game
     * @return list of players according to the amount of human players and cpu asked for by the parameter
     */
    public ArrayList<Player> createPlayers(int numberOfHumanPlayers, int numberOfCPU, Game game) {
        ArrayList<Player> players = new ArrayList<>();

        for (int i = 0; i < numberOfHumanPlayers; i++) {
            players.add(new HumanPlayer("", new ArrayList<>(), this, game));
        }

        for (int i = 0; i < numberOfCPU; i++) {
            players.add(new ComputerPlayer("CPU", new ArrayList<>(), this, game));
        }

        return players;
    }

    /**
     * Shows the top three cards when see future card is played (on index 0, 1 and 2)
     */
    public void showCardsDeck(){
        System.out.println("The first three cards are " + cardsDeck.get(0).getCardType() + ", " + cardsDeck.get(1).getCardType() + " and " +
                cardsDeck.get(2).getCardType());

    }

    /**
     * Creates a new deck based on the number of players (this is to determine how many defuse cards there are to be in
     * the deck)
     * @param numberOfPlayers
     * @return
     */
    private ArrayList<Card> createDeck(int numberOfPlayers) {
        ArrayList<Card> newDeck = new ArrayList<>();

        //Add defuse cards
        int numberOfDefuse = calculateDefuseCards(numberOfPlayers);
        for (int i = 0; i < numberOfDefuse; i++) {
            newDeck.add(new Card(CardCategory.DEFUSE));
        }

        //Add attack
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.ATTACK));
        }

        //Add favor
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.FAVOR));
        }

        //Add nope
        for (int i = 0; i < 5; i++) {
            newDeck.add(new Card(CardCategory.NOPE));
        }

        //Add shuffle
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.SHUFFLE));
        }

        //Add skip
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.SKIP));
        }

        //Add see the future
        for (int i = 0; i < 5; i++) {
            newDeck.add(new Card(CardCategory.SEE_THE_FUTURE));
        }

        //Add tacocat
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.TACO_CAT));
        }

        //Add cattermelon
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.CATTER_MELON));
        }

        //Add potatocat
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.HAIRY_POTATO_CAT));
        }

        //Add beardcap
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.BEARD_CAT));
        }

        //Add rainbowcat
        for (int i = 0; i < 4; i++) {
            newDeck.add(new Card(CardCategory.RAINBOW_RALPHING_CAT));
        }

        return newDeck;

    }

    /**
     * Calculates how many exploding kittens are to be added in the deck, always one less than the total amount of players.
     * @param numberOfPlayers
     * @return
     */
    private int calculateExplodingKittens(int numberOfPlayers) {
        return numberOfPlayers - 1;
    }

    /**
     * Calculates how many defuse cards there are to be added to the deck. If there are 2 or three players, 2 are added.
     * Else, 6 - the amount of players is added (because the players all get one).
     * @param numberOfPlayers
     * @return
     */
    private int calculateDefuseCards(int numberOfPlayers) {
        if (numberOfPlayers == 2 || numberOfPlayers == 3) {
            return 2;
        } else {
            return 6 - numberOfPlayers;
        }
    }

    /**
     * Draws a card from the deck. It removes the card on index 0 and decreases the size of the deck by 1.
     * @return
     */
    public Card drawCard() {
        if (!cardsDeck.isEmpty()) {
            setSize(size - 1);
            return cardsDeck.remove(0);
        } else {
            return null;
        }
    }

    public ArrayList<Card> getCardsDeck() {
        return cardsDeck;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Prints the entire deck
     */
    public void printDeck() {
        for (int i = 0; i < this.getCardsDeck().size(); i++) {
            System.out.println(this.getCardsDeck().get(i).getCardType());
        }
    }

    /**
     * Resets the deck to create a new deck for the number of players and shuffles again.
     */
    public void reset() {
        createDeck(numberOfPlayers);
        shuffle();
    }
}
