package group15.explodingkittens.Model.Player;


import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Move.MoveType;

import java.util.ArrayList;
import java.util.Scanner;

public class HumanPlayer extends Player {
    private CardCategory drawnCard;

    /**
     * Creates a new Player object
     *
     * @param name
     * @param cardsInHand
     * @param deck
     * @param game
     * @requires name is not null
     * @requires cardsInHand.hasThisCard(Card.Type.DEFUSE) and !cardsInHand.hasThisCard(Card.Type.EXPLODING_KITTEN)
     * @ensures the Name of the player will be name
     * @ensures the player will not have an EK (exploding kitten) card and will have a DEFUSE
     */
    private boolean isHuman;
    public HumanPlayer(String name, ArrayList<Card> cardsInHand, Deck deck, Game game) {
        super(name, cardsInHand, deck, game);
        this.isHuman = true;
    }

    /**
     * Asks for the player to decide on a new move.
     * @return Move
     */
    @Override
    public Move determineMove() {
        System.out.println("Please choose your type of move:");
        System.out.println("1. Draw a card, turn is over");
        System.out.println("2. Play a card");
        System.out.println("3. Play 2 of the same cards");
        System.out.println("4. Play 3 of the same cards");


        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();


        switch (choice) {
            case 1:
                return new Move(MoveType.DRAW, this);
            case 2:
                return new Move(MoveType.PLAY, chooseCard(), this);
            case 3:
                if (hasMoreOfSame()) {
                    return new Move(MoveType.TWO, chooseNCards(2), this);
                } else {
                    System.out.println("Invalid choice. You are drawing a card.");
                    return new Move(MoveType.DRAW, this);
                }
            case 4:
                if (hasMoreOfSame()) {
                    return new Move(MoveType.THREE, chooseNCards(3), this);
                } else {
                    System.out.println("Invalid choice. You are drawing a card.");
                    return new Move(MoveType.DRAW, this);
                }
            default:
                System.out.println("Invalid choice, you are drawing a card");
                return new Move(MoveType.DRAW, this);
        }

    }

    /**
     * When user chooses to play a card, this method is called. It asks the user to choose the card they want to play.
     * @return Chosen card to play
     */
    @Override
    public Card chooseCard() {
        System.out.println("Please choose a card to play, please choose the index (keep in mind indexes start with 0).");

        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();

        return getCardsInHand().get(index);
    }

    /**
     * Used by the choose2Cards and choose3Cards methods. Used to ask the user for either two or three cards.
     * @param n 2 or 3
     * @return list with chosen cards by the user.
     */
    @Override
    public ArrayList<Card> chooseNCards(int n) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Card> result = new ArrayList<>();


        for (int i = 0; i < n; i++) {
            System.out.println("Please choose card " + (i + 1) + " (enter index): ");
            String input = scanner.next();


            int index;

            try {
                index = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a valid index.");
                i--;
                continue;
            }


            if (index < 0 || index >= getCardsInHand().size()) {
                System.out.println("Invalid index. Please choose again.");
                i--; // Retry for the same card
                continue;
            }


            Card chosenCard = getCardsInHand().get(index);


            if (i > 0 && !chosenCard.getCardType().equals(result.get(0).getCardType())) {
                System.out.println("Chosen cards must be of the same type. Please choose again.");
                i--; // Retry for the same card
                continue;
            }


            result.add(chosenCard);
        }


        return result;
    }

    /**
     * Player draws a card from the deck in parameter.
     * @param deck deck player draws from
     */
    @Override
    public void drawCard(Deck deck) {
        Card drawnCard = deck.drawCard(); //Gets card from deck
        this.drawnCard = drawnCard.getCardType();

        if (drawnCard.getCardType() == CardCategory.EXPLODING_KITTEN && !hasThisCard(CardCategory.DEFUSE)) {
            //If drawn an exploding kitten and doesn't have defuse, they blow up
            blowsUp();
        } else if (drawnCard.getCardType() == CardCategory.EXPLODING_KITTEN && hasThisCard(CardCategory.DEFUSE)) {
            //If they do draw an exploding kitten and have a defuse, message is written and exploding kitten is put back
            // into the draw pile. Defuse card is taken from the player's hand.

            System.out.println("You pulled an exploding kitten, but had a defuse card :). Please return the card in the " +
                    "pile, please provide an index to return into the draw pile.");

            Scanner scanner = new Scanner(System.in);
            int index = scanner.nextInt();

            deck.getCardsDeck().add(index, drawnCard);
            getCardsInHand().removeIf(card -> card.getCardType() == CardCategory.DEFUSE);

        } else {
            System.out.println("Drew a card: " + drawnCard.getCardType());
            getCardsInHand().add(drawnCard);
            this.endTurn();
        }
    }

    /**
     * Plays the chosen two cards in chooseNCards() method. Adds random card from other player. Since there is only
     * one other player, there is no need to ask for which other player the current player wants to steal from.
     * @param cards two cards of the same type chosen by the user in other method.
     */
    @Override
    public void play2Cards(ArrayList<Card> cards) {

        if (cards.get(0).getCardType() == cards.get(1).getCardType()) {
            Player chosen;

            if (getGame().getCurrent() == 1) {
                chosen = getGame().getActivePlayers().get(0);
            } else {
                chosen = getGame().getActivePlayers().get(1);
            }

            Card result = chosen.getCardsInHand().get(1 + (int)(Math.random() * ((chosen.getCardsInHand().size() - 1) + 1)));


            chosen.getCardsInHand().remove(result);
            getCardsInHand().add(result);

            this.getCardsInHand().remove(cards.get(0));
            this.getCardsInHand().remove(cards.get(1));

            System.out.println("Your added card is: " + result.getCardType());
        }

    }

    /**
     * Plays the chosen two cards in chooseNCards() method. Adds certain type card from other player.
     * If they obtain that card, the current player gets it. Otherwise, sucks for the better luck next time.
     * Since there is only one other player, there is no need to ask for which other player the current player wants
     * to steal from.
     * @param cards two cards of the same type chosen by the user in other method.
     */
    @Override
    public void play3Cards(ArrayList<Card> cards) {
        if ((cards.get(0).getCardType() == cards.get(1).getCardType()) && cards.get(0).getCardType() == cards.get(2).getCardType()) {
            System.out.println("Please choose player integer:");
            Scanner scanner = new Scanner(System.in);
            int input = scanner.nextInt();
            Player chosenPlayer = getGame().getActivePlayers().get(input);


            System.out.println("Please choose card type:" +
                    "DEFUSE - d\n" +
                    "ATTACK - a\n" +
                    "SEE_THE_FUTURE - stf\n" +
                    "SKIP - s\n" +
                    "FAVOR - f\n" +
                    "SHUFFLE - shuffle\n" +
                    "TACO_CAT - t\n" +
                    "BEARD_CAT - b\n" +
                    "CATTER_MELON - cm\n" +
                    "HAIRY_POTATO_CAT - hp\n" +
                    "RAINBOW_RALPHING_CAT - rr\n");
            Scanner scanner1 = new Scanner(System.in);
            String chosenType = scanner1.next();


            switch (chosenType) {
                case "d":
                    if (chosenPlayer.hasThisCard(CardCategory.DEFUSE)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.DEFUSE);
                        this.getCardsInHand().add(new Card(CardCategory.DEFUSE));
                    }
                    break;
                case "a":
                    if (chosenPlayer.hasThisCard(CardCategory.ATTACK)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.ATTACK);
                        this.getCardsInHand().add(new Card(CardCategory.ATTACK));
                    }
                    break;
                case "stf":
                    if (chosenPlayer.hasThisCard(CardCategory.SEE_THE_FUTURE)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.SEE_THE_FUTURE);
                        this.getCardsInHand().add(new Card(CardCategory.SEE_THE_FUTURE));
                    }
                    break;
                case "s":
                    if (chosenPlayer.hasThisCard(CardCategory.SKIP)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.SKIP);
                        this.getCardsInHand().add(new Card(CardCategory.SKIP));
                    }
                    break;
                case "f":
                    if (chosenPlayer.hasThisCard(CardCategory.FAVOR)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.FAVOR);
                        this.getCardsInHand().add(new Card(CardCategory.FAVOR));
                    }
                    break;
                case "shuffle":
                    if (chosenPlayer.hasThisCard(CardCategory.SHUFFLE)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.SHUFFLE);
                        this.getCardsInHand().add(new Card(CardCategory.SHUFFLE));
                    }
                    break;
                case "t":
                    if (chosenPlayer.hasThisCard(CardCategory.TACO_CAT)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.TACO_CAT);
                        this.getCardsInHand().add(new Card(CardCategory.TACO_CAT));
                    }
                    break;
                case "b":
                    if (chosenPlayer.hasThisCard(CardCategory.BEARD_CAT)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.BEARD_CAT);
                        this.getCardsInHand().add(new Card(CardCategory.BEARD_CAT));
                    }
                    break;
                case "cm":
                    if (chosenPlayer.hasThisCard(CardCategory.CATTER_MELON)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.CATTER_MELON);
                        this.getCardsInHand().add(new Card(CardCategory.CATTER_MELON));
                    }
                    break;
                case "hp":
                    if (chosenPlayer.hasThisCard(CardCategory.HAIRY_POTATO_CAT)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.HAIRY_POTATO_CAT);
                        this.getCardsInHand().add(new Card(CardCategory.HAIRY_POTATO_CAT));
                    }
                    break;
                case "rr":
                    if (chosenPlayer.hasThisCard(CardCategory.RAINBOW_RALPHING_CAT)) {
                        chosenPlayer.getCardsInHand().remove(CardCategory.RAINBOW_RALPHING_CAT);
                        this.getCardsInHand().add(new Card(CardCategory.RAINBOW_RALPHING_CAT));
                    }
                    break;
                default:
                    System.out.println("Please input a valid type.");


            }

        }
    }

    /**
     * Plays the favor card. Other player has to choose a card to give away. This card is removed from their hand and
     * added to the current player.
     */
    @Override
    public void playFavor() {
        try {
            System.out.println("Choose player index: ");
            System.out.println("Player indexes start from 0 and go in the order you gave the names in the beginning. " +
                    "Chosen Computer players are automatically added after Human players.");


            Scanner scanner = new Scanner(System.in);
            int index = scanner.nextInt();


            Player chosenPlayer = getGame().getActivePlayers().get(index);


            System.out.println(chosenPlayer.getName() + ": you have to give away a card, please choose and integer.");
            Scanner scanner1 = new Scanner(System.in);
            int index1 = scanner1.nextInt();


            Card chosenCard = chosenPlayer.getCardsInHand().get(index1);
            chosenPlayer.removeCardFromHand(chosenCard);
            this.addToHand(chosenCard);


        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CardCategory getDrawnCard() {
        return drawnCard;
    }
}
