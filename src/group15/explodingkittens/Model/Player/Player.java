package group15.explodingkittens.Model.Player;

import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Move.Move;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Player class. Superclass of HumanPlayer and ComputerPlayer
 */
public abstract class Player {
    private String name;
    private ArrayList<Card> cardsInHand;
    private Deck deck;
    private boolean turnSkipped;
    private boolean alive;
    private Game game;
    private boolean isHuman;
    private boolean isAttacked;
    private int turns;
    private CardCategory drawnCard;
    private boolean isExploded;
    //    ----CONSTRUCTOR-------------------------------------------------------------------------------------------

    /**
     * Creates a new Player object
     * Turn is by default not skipped, alive and not attacked.
     *
     * @requires name is not null
     * @requires cardsInHand.hasThisCard(Card.Type.DEFUSE) and !cardsInHand.hasThisCard(Card.Type.EXPLODING_KITTEN)
     * @ensures the Name of the player will be name
     * @ensures the player will not have an EK (exploding kitten) card and will have a DEFUSE
     *
      */
    public Player(String name, ArrayList<Card> cardsInHand, Deck deck, Game game){
        this.name = name;
        this.cardsInHand = cardsInHand;
        this.deck = deck;
        this.turnSkipped = false;
        this.alive = true;
        this.game = game;
        this.isAttacked = false;
        this.turns = 0;
        this.isExploded = false;
    }


//    ----TAKE A TURN-------------------------------------------------------------------------------------------

    /**
     * Takes a turn. When not skipped, the player can take a turn. Else, turn is ended without determining and processing
     * a move.
     */
    public void takeTurn() {

        if (!isTurnSkipped()) {
            System.out.println(name + ": it is your turn! " + handToString());

            Move move = determineMove();
            processMove(move);

        } else {
            System.out.println("Sorry, you are skipped :(");
            endTurn();
        }

    }

//    ---DETERMINE MOVE-------------------------------------------------------------------------------------------

    /**
     * Player determines what will be the next move.
     * @return
     */
    public abstract Move determineMove();

    /**
     * When chosen option 2 to play one card, this method asks which card the player wants to use.
     * @return
     */
    public abstract Card chooseCard();

    /**
     * Chooses 2 or 3 cards
     * @param n 2 or 3
     * @return List of chosen cards
     */
    public abstract ArrayList<Card> chooseNCards(int n);

    /**
     * Checks if player has more cards of the same type in their hand.
     * @return
     */
    public boolean hasMoreOfSame() {
        Set<CardCategory> seenTypes = new HashSet<>();

        for (Card card : cardsInHand) {
            if ((!seenTypes.add(card.getCardType()))) {
                return true;
            }
        }

        return false;
    }
//    ----PROCESS MOVE-------------------------------------------------------------------------------------------

    /**
     * Processes a move chosen in the determineMove() class.
     * @param move
     */
    public void processMove(Move move) {
        switch (move.getMoveType()) {
            case DRAW:
                drawCard(getDeck());
                break;
            case PLAY:
                playCard(move.getCard());
                Move move2 = determineMove();
                processMove(move2);
                break;
            case TWO:
                play2Cards(move.getCards());
                Move move3 = determineMove();
                processMove(move3);
                break;
            case THREE:
                play3Cards(move.getCards());
                Move move4 = determineMove();
                processMove(move4);
                break;
        }
    }

    /**
     * Draws a card from the deck.
     * @param deck
     */
    public abstract void drawCard(Deck deck);

    /**
     * Plays a card, invoked when processMove receives a move of type PLAY
     * @param card
     */
    private void playCard(Card card){
        switch (card.getCardType()) {
            case ATTACK:
                if (hasThisCard(CardCategory.ATTACK)) {
                    playAttack();
                    removeCardFromHand(card);
                }
                break;
            case FAVOR:
                if (hasThisCard(CardCategory.FAVOR)) {
                    playFavor();
                    removeCardFromHand(card);
                }
                break;
            case SHUFFLE:
                if (hasThisCard(CardCategory.SHUFFLE)) {
                    deck.shuffle();
                    removeCardFromHand(card);
                }
                break;
            case SKIP:
                if (hasThisCard(CardCategory.SKIP)) {
                    playSkip();
                    removeCardFromHand(card);
                }
                break;
            case SEE_THE_FUTURE:
                if (hasThisCard(CardCategory.SEE_THE_FUTURE)) {
                    deck.showCardsDeck();
                    removeCardFromHand(card);
                }
                handToString();
                break;

        }


    }

    /**
     * Plays the chosen two cards in chooseNCards() method. Adds random card from other player. Since there is only
     * one other player, there is no need to ask for which other player the current player wants to steal from.
     * @param cards two cards of the same type chosen by the user in other method.
     */
    public abstract void play2Cards(ArrayList<Card> cards);

    /**
     * Plays the chosen two cards in chooseNCards() method. Adds certain type card from other player.
     * If they obtain that card, the current player gets it. Otherwise, sucks for the better luck next time.
     * Since there is only one other player, there is no need to ask for which other player the current player wants
     * to steal from.
     * @param cards two cards of the same type chosen by the user in other method.
     */
    public abstract void play3Cards(ArrayList<Card> cards);

    /**
     * Checks if a players turn is skipped
     * @return true if skipped, false if not skipped
     */
    public boolean isTurnSkipped() {
        return turnSkipped;
    }

    public void setTurnSkipped(boolean turnSkipped) {
        this.turnSkipped = turnSkipped;
    }

    /**
     * Ends turn after skipped
     * @ensures turnSkipped = false
     */
    public void endTurn() {
        turnSkipped = false;
    }

    /**
     * Plays an attack card, next player will have two turns.
     */
    private void playAttack() {
        Player currentPlayer = game.getActivePlayers().get(game.getCurrent());
        Player nextPlayer = game.getActivePlayers().get((game.getCurrent() + 1) % game.getNUMBER_PLAYERS());
        System.out.println("You played an Attack card! " + nextPlayer.getName() + " will have two turns.");

        nextPlayer.setAttacked(true);

        currentPlayer.endTurn();
        nextPlayer.takeTurn();

        game.moveToNextPlayer();
    }

    /**
     * Plays skip card, next player will be skipped, after this players turn ends, it is their turn again because there
     * are two players.
     */
    private void playSkip() {
        Player currentPlayer = game.getActivePlayers().get((game.getCurrent()));
        Player nextPlayer = game.getActivePlayers().get((game.getCurrent() + 1) % game.getNUMBER_PLAYERS());

        System.out.println("You played a Skip card! " + nextPlayer.getName() + "'s turn is skipped.");

        currentPlayer.endTurn();
        nextPlayer.setTurnSkipped(true);
//        nextPlayer.endTurn();
    }

    /**
     * Plays the favor card. Other player has to choose a card to give away. This card is removed from their hand and
     * added to the current player.
     */
    public abstract void playFavor();

    //    ----CHECKS AND OTHER METHODS-------------------------------------------------------------------------------------------

    /**
     * Checks if the player has this card in their hand.
     * @param cardType type to be checked
     * @return true if has this card, false if not
     */
    public boolean hasThisCard(CardCategory cardType){
        for(Card card : cardsInHand){
            if(card.getCardType() == cardType){
                return true;
            }
        }
        return false;
    }

    /**
     * Adds card to player hand
     * @param card card to be added
     */
    public void addToHand(Card card){
        cardsInHand.add(card);
    }

    /**
     * Removes the card from player hand
     * @param card card to be deleted
     */
    public void removeCardFromHand(Card card){
        cardsInHand.remove(card);
    }

    /**
     * Prints out the cards in the hand.
     * @return string with cards in hand
     */
    public String handToString() {
        String result = "Your current cards are: \n";

        for (Card card : cardsInHand) {
            String currentCard = String.valueOf(card.getCardType());
            result = result + currentCard + "\n";
        }

        return result.replaceAll(", $", "");
    }

    /**
     * Blows up a player, only invoked when player draws an exploding kitten and does not have a defuse card.
     * @requires drawnCard.getType() = EXPLODING_KITTEN && !hasThisCard.DEFUSE
     */
    public void blowsUp() {
        System.out.println("BOOM");
        alive = false;
        setExploded(true);
        game.playerEliminated(this);
        System.out.println("Remaining players are: ");
        for (int i = 0; i < game.getActivePlayers().size(); i++) {
            System.out.println(game.getActivePlayers().get(i).getName());
        }
    }

    /**
     * Resets the player to default. Player is alive and not attacked.
     */
    public void reset() {
        alive = true;
        setAttacked(false);
    }

//    ----GETTERS AND SETTERS-------------------------------------------------------------------------------------------

    /**
     * Returns the name of the player
     */
    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns cards of the player
     * @return list of cards in hand
     */
    public ArrayList<Card> getCardsInHand() {
        return cardsInHand;
    }

    public void setCardsInHand(ArrayList<Card> cardsInHand) {
        this.cardsInHand = cardsInHand;
    }

    public void setAttacked(boolean attacked) {
        isAttacked = attacked;
    }

    public Game getGame() {
        return game;
    }

    public Deck getDeck() {
        return deck;
    }

    public CardCategory getDrawnCard() {
        return drawnCard;
    }

    public boolean isExploded() {
        return isExploded;
    }

    public void setExploded(boolean exploded) {
        isExploded = exploded;
    }
}
