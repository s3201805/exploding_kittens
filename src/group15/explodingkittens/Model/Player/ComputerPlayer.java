package group15.explodingkittens.Model.Player;

import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Move.MoveType;

import java.util.ArrayList;

/**
 * Class for a ComputerPlayer, subclass of Player class.
 */
public class ComputerPlayer extends Player {
    private boolean isHuman;

    /**
     * Creates a new ComputerPlayer object
     * @param name
     * @param cardsInHand
     * @param deck
     * @param game
     */
    public ComputerPlayer(String name, ArrayList<Card> cardsInHand, Deck deck, Game game) {
        super(name, cardsInHand, deck, game);
        this.isHuman = false;
    }

    /**
     * General method to determine the next move. Only plays valid actions, if the player has a SEE_THE_FUTURE type
     * card, this is played. Same for ATTACK, else a card is drawn.
     * @return
     */
    @Override
    public Move determineMove() {
        for (Card card : getCardsInHand()) {
            if (card.getCardType() == CardCategory.SEE_THE_FUTURE) {
                return new Move(MoveType.PLAY, card, this);

            } else if (card.getCardType() == CardCategory.ATTACK) {
                return new Move(MoveType.PLAY, card, this);
            }
        }

        return new Move(MoveType.DRAW, this);
    }

    @Override
    public Card chooseCard() {
        return null;
    }

    @Override
    public ArrayList<Card> chooseNCards(int n) {
        return null;
    }

    /**
     * Draws a card from given deck in the parameter
     * @param deck
     */
    @Override
    public void drawCard(Deck deck) {
        Card drawnCard = deck.drawCard();

        if (drawnCard.getCardType() == CardCategory.EXPLODING_KITTEN && !hasThisCard(CardCategory.DEFUSE)) {
            blowsUp();
        } else if (drawnCard.getCardType() == CardCategory.EXPLODING_KITTEN && hasThisCard(CardCategory.DEFUSE)) {

            System.out.println("CPU drew an exploding kitten, but had a defuse card. ");

            deck.getCardsDeck().add(0, drawnCard);
            this.removeCardFromHand(drawnCard);

            getCardsInHand().removeIf(card -> card.getCardType() == CardCategory.DEFUSE);

            deck.printDeck();

        }

        System.out.println("Drew a card: " + drawnCard.getCardType());
        getCardsInHand().add(drawnCard);

    }

    @Override
    public void play2Cards(ArrayList<Card> cards) {
    }

    @Override
    public void play3Cards(ArrayList<Card> cards) {
    }

    @Override
    public void playFavor() {
    }


}
