package group15.explodingkittens.Model.tests;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {
    @Test
    public void testGame(){
        Game game = new Game(2, 0, new Deck(2));
        String in = "2\nAmy Alice\n";
        InputStream inputStream = new ByteArrayInputStream(in.getBytes());
        System.setIn(inputStream);
        game.start();
        ArrayList<Player> activePlayers = game.getActivePlayers();
        assertEquals(1, activePlayers.size());
        assertEquals("Amy", activePlayers.get(0).getName());
    }

}
