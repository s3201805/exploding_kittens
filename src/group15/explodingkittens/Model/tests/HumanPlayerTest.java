package group15.explodingkittens.Model.tests;
import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.HumanPlayer;
import group15.explodingkittens.Model.Player.ComputerPlayer;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Move.MoveType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

public class HumanPlayerTest {
    /**
     * This method tests method determineMove()  when a card is being drawn
     */
    @Test
    public void testDetermineMoveDrawCard(){
        HumanPlayer humanPlayer = new HumanPlayer("Human Player", new ArrayList<>(), new Deck(2), new Game(2, 0, new Deck(2) ));
        String in = "1\n";
        InputStream inputStream = new ByteArrayInputStream(in.getBytes());
        System.setIn(inputStream);
        Move playerMove = humanPlayer.determineMove();
        assertEquals(MoveType.DRAW, playerMove.getMoveType());
    }


    /**
     * This method tests method determineMove() when a card is being played
     */
    @Test
    public void testDetermineMovePlayCard(){
        HumanPlayer humanPlayer = new HumanPlayer("Human Player", new ArrayList<>(), new Deck(2), new Game(2, 0, new Deck(2)));
        String in = "2\n0\n";
        InputStream inputStream = new ByteArrayInputStream(in.getBytes());
        System.setIn(inputStream);
        humanPlayer.addToHand(new Card(CardCategory.BEARD_CAT));

        Move playerMove = humanPlayer.determineMove();
        assertEquals(MoveType.PLAY, playerMove.getMoveType());
        assertEquals(CardCategory.BEARD_CAT, playerMove.getCard().getCardType());
    }

    @Test
    public void testChooseCard(){
      HumanPlayer humanPlayer = new HumanPlayer("Human Player", new ArrayList<>(), new Deck(2), new Game(2, 0, new Deck(2)));
      String in = "1\n";
      InputStream inputStream = new ByteArrayInputStream(in.getBytes());
      System.setIn(inputStream);

      humanPlayer.addToHand(new Card(CardCategory.BEARD_CAT));
      humanPlayer.addToHand(new Card(CardCategory.FAVOR));
      humanPlayer.addToHand(new Card(CardCategory.TACO_CAT));

      Card chosenCard = humanPlayer.chooseCard();
      assertEquals(CardCategory.TACO_CAT, chosenCard.getCardType());
    }

    @Test
    public void testDetermineMoveWithInvalidInput(){
        HumanPlayer humanPlayer = new HumanPlayer("Human Player", new ArrayList<>(), new Deck(2), new Game(2, 0, new Deck(2)));
        String in = "invalid input\n1\n";
        InputStream inputStream = new ByteArrayInputStream(in.getBytes());
        System.setIn(inputStream);
        Move playerMove = humanPlayer.determineMove();
        assertEquals(MoveType.DRAW, playerMove.getMoveType());
    }

    @Test
    public void testChooseNCards(){
        HumanPlayer humanPlayer = new HumanPlayer("Human Player", new ArrayList<>(), new Deck(2), new Game(2, 0, new Deck(2)));
        String in = "0\n2\n";
        InputStream inputStream = new ByteArrayInputStream(in.getBytes());
        System.setIn(inputStream);

        humanPlayer.addToHand(new Card(CardCategory.ATTACK));
        humanPlayer.addToHand(new Card(CardCategory.TACO_CAT));
        humanPlayer.addToHand(new Card(CardCategory.TACO_CAT));
        humanPlayer.addToHand(new Card(CardCategory.SKIP));
        ArrayList<Card> chosenCards = humanPlayer.chooseNCards(2);

        assertEquals(2, chosenCards.size());
        assertEquals(CardCategory.TACO_CAT, chosenCards.get(0).getCardType());
        assertEquals(CardCategory.TACO_CAT, chosenCards.get(1).getCardType());
    }
}
