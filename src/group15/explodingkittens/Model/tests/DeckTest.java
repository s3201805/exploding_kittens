package group15.explodingkittens.Model.tests;

import group15.explodingkittens.Model.Player.ComputerPlayer;
import group15.explodingkittens.Model.Player.HumanPlayer;
import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckTest {
    @Test
    public void testDrawCard(){
        Deck deck = new Deck(2);
        int initialSize = deck.getSize();
        Card drawnCard = deck.drawCard();

        assertNotNull(drawnCard);
        assertEquals(initialSize-1, deck.getSize());
    }

    @Test
    public void testDrawCardWithEmptyDeck(){
        Deck deck = new Deck(2);
        assertNull(deck.drawCard());
    }

    @Test
    public void testShuffle(){
        Deck deck = new Deck(2);
        ArrayList<Card> initialDeck = new ArrayList<>(deck.getCardsDeck());
        deck.shuffle();
        ArrayList<Card> shuffledDeck = deck.getCardsDeck();
        assertNotEquals(initialDeck, shuffledDeck);
    }

    @Test
    public void testDistribute(){
        Deck deck = new Deck(2);
        ArrayList<Player> players = new ArrayList<>();
        players.add(new HumanPlayer("Player1", new ArrayList<>(), deck, new Game(2,0,deck)));
        players.add(new ComputerPlayer("Computer Player", new ArrayList<>(), deck, new Game(2,0,deck)));
        deck.distribute(players);
        for(Player player : players){
            assertEquals(7, player.getCardsInHand().size());
            assertTrue(player.hasThisCard(CardCategory.DEFUSE));
        }
    }
}
