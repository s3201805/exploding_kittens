package group15.explodingkittens.Model.tests;
import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.HumanPlayer;
import group15.explodingkittens.Model.Player.ComputerPlayer;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ExplodingKittensSystemTest {
    @Test
    public void testGameBasics(){
        Game game = new Game(1, 1, new Deck(2));

        assertEquals(2, game.getActivePlayers().size());
        assertNotNull(game.getActivePlayers().get(0).getName());
        assertNotNull(game.getActivePlayers().get(1).getName());

        for(int i = 0; i< 5 ; i++){
           // game.playTurn();
        }
        assertTrue(game.isContinueGame());
        assertNotNull(game.getCurrentPlayer());
        assertNotNull(game.getDeck());
    }

}
