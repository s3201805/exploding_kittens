package group15.explodingkittens.Model.tests;
import group15.explodingkittens.Controller.ClientServer.Client.Client;
import group15.explodingkittens.View.client.ClientTUI;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class ClientServerSystemTest {
    @Test
    public void testClientServerInteraction(){
        Client gameCLient = new Client();
        ClientTUI clientTUI = new ClientTUI(gameCLient);
        Thread clientThread = new Thread(new Runnable() {
            @Override
            public void run() {
                clientTUI.start();
            }
        });
        clientThread.start();
        try{
            clientTUI.handleUserInput("PC");
        }catch (Exception exception){
            fail("Exception occured during interaction" + exception.getMessage());
        }
        clientThread.interrupt();
        assertFalse(clientThread.isAlive());
    }

}
