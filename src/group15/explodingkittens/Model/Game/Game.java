package group15.explodingkittens.Model.Game;

import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Player.Player;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Game {


    /**
     * Index of the current player.
     * @invariant the index is always between 0 and NUMBER_PLAYERS
     */
    private int current;

    /**
     * Number of players, for now this is 2.
     */
    public int NUMBER_PLAYERS;

    /**
     * List of the active players, when a player dies, they are removed from this list.
     */
    private ArrayList<Player> activePlayers;

    private Deck deck;
    private boolean continueGame; //Decides whether to end the program or not.



    //Constructor
    /**
     * Creates a new Game object.
     */

    public Game(int numberOfHumanPlayers, int numberCPU, Deck deck) {
        NUMBER_PLAYERS = numberOfHumanPlayers + numberCPU;
        this.deck = deck;
        this.activePlayers = deck.createPlayers(numberOfHumanPlayers, numberCPU, this);
        this.current = 0;
        this.continueGame = true;
    }

//    public Game(String name){
//        Player admin = new HumanPlayer(name);
//        activePlayers = new ArrayList<>();
//        activePlayers.add(admin);
//        addPlayers();
//        this.logic = new GameLogic();
//        this.logic.setGame(this);
//        this.table = new Table(this, players);
//
//    }

    /**
     * When a player is eleminated, they are removed from the activePlayers list.
     * @param player
     */
    public void playerEliminated(Player player) {
        activePlayers.remove(player);
    }

    /**
     * Looks if there is a winner, if the list of active players is 1, this means there is only 1 player left, this
     * player is the winner.
     * @return
     */
    public String determineWinner() {
        if (activePlayers.size() == 1) {
            return activePlayers.get(0).getName();
        } else {
            return null;
            // there is no winner yet
        }
    }

    /**
     * Starts a game
     */
    public void start() {

        while (continueGame) {
            reset();

            deck.shuffle();

            play();

            boolean validInput = false;

            while (!validInput) {
                System.out.println("\n> Play another time? (true/false)?");


                try {
                    Scanner scanner = new Scanner(System.in);
                    continueGame = Boolean.parseBoolean(scanner.nextLine());
                    validInput = true;
                } catch (InputMismatchException e) {
                    System.out.println("Invalid input. Please enter 'true' or 'false'.");
                }
            }


        }

    }

    /**
     * Resets the game. The current player index is set to 0. Players are reset and the deck is reset and the deck
     * redistributes. Announcement is made that the game is reset for a new round.
     */
    public void reset() {
        for (Player player : activePlayers) {
            player.reset();
        }

        current = 0;
        deck.reset();

        deck.distribute(activePlayers);
        System.out.println("Game reset for a new round.");
    }

    /**
     * Plays the Exploding Kittens game. <br>
     * First the cards are dealt and the cards are privately shown. Then the game is played
     * until only one player is left (gameOver()). Players can make a move one after the other.
     * After each move, the changed deck size is shown.
     */
    private void play() {
        while (!gameOver()) {
            Player currentPlayer = activePlayers.get(current); //Get current player
            System.out.println("It's " + currentPlayer.getName() + "'s turn!");

            if (!currentPlayer.isTurnSkipped()) { //If they are not skipped by previous player, continue

                    System.out.println(currentPlayer.getName() + ": it is your turn! " + currentPlayer.handToString());

                    Move move = currentPlayer.determineMove(); //Decide on what move to do
                    currentPlayer.processMove(move); //Process this move

            } else { //Current player is skipped
                System.out.println("Sorry, you are skipped :(");
                currentPlayer.endTurn();
            }

            if (gameOver()) {
                printResult();
                break;
            }

            current += 1;
            if (current == NUMBER_PLAYERS) {
                current = 0;
            }

            update(); //Shows the deck size

        }
    }

    /**
     * Returns if there is a winner.
     * @return
     */
    public boolean gameOver() {
        return determineWinner() != null;
    }

    /**
     * Prints Deck size
     */
    public void update() {
        System.out.println("Deck size: " + deck.getSize());
    }

    /**
     * Prints the result of the last game. <br>
     * @requires the game to be over
     */
    public void printResult() {
        if (this.determineWinner() != null) {
            String winner = this.determineWinner();
            System.out.println(winner + " has won!");
        }
    }

    /**
     * Returns current player index.
     * @return
     */
    public int getCurrent() {
        return current;
    }

    /**
     * Returns the number of players in the game.
     * @return
     */
    public int getNUMBER_PLAYERS() {
        return NUMBER_PLAYERS;
    }

    /**
     * Returns a list of the current active players.
     * @return
     */
    public ArrayList<Player> getActivePlayers() {
        return activePlayers;
    }

    /**
     * Moves the turn to the next player.
     */
    public void moveToNextPlayer() {
        current = (current + 1) % getActivePlayers().size();
    }

    public Deck getDeck() {
        return deck;
    }

    public boolean isContinueGame() {
        return continueGame;
    }

    public void setContinueGame(boolean continueGame) {
        this.continueGame = continueGame;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public void setActivePlayers(ArrayList<Player> activePlayers) {
        this.activePlayers = activePlayers;
    }

    public Player getCurrentPlayer() {
        return this.getActivePlayers().get(current);
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}
