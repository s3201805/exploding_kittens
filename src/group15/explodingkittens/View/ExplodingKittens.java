package group15.explodingkittens.View;

import group15.explodingkittens.Model.Deck.Deck;
import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Player.Player;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExplodingKittens {
    /**
     * Controls the interaction between the Players and the Table and
     * shows a textual representation of the Deck in the console.
     *
     * IMPORTANT: Creates a new game based on the input of the user. Total number of players can only be 2. This way when the user
     * inputs 1, a new game is started with a HumanPlayer and a ComputerPlayer. Else if the input is 2, the game is started
     * with 2 HumanPlayers.
     *
     * @param args
     */

    public static void main(String[] args) {
        int amountHuman;
        int amountCPU;

        System.out.println("Welcome! Please input how many HumanPlayers (1 or 2, total amount of players can only be 2): ");
        Scanner scanner = new Scanner(System.in);

        try {
            int input = scanner.nextInt();

            if (input <= 2 && input > 0) {
                amountHuman = input;
                if (input == 1) {
                    amountCPU = 1;
                } else {
                    amountCPU = 0;
                }
            } else {
                System.out.println("Invalid integer. Please input 1 or 2.");
                return;
            }

            //Asks for names for the human players, CPu are named CPU
            System.out.println("Please insert names for your human players: ");
            Scanner scanner2 = new Scanner(System.in);

            Game game = null;
            try {
                String[] names = scanner2.nextLine().split(" ");

                //Makes a new game
                game = new Game(amountHuman, amountCPU, new Deck(amountCPU + amountHuman));
                ArrayList<Player> players = game.getActivePlayers();

                //Gives given names
                players.get(0).setName(names[0]);

                if (names.length == 2) {
                    players.get(1).setName(names[1]);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Not enough names provided");
            }

            //Starts the game
            game.start();

        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please input a valid integer.");
        }


    }
}
