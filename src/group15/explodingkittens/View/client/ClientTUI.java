package group15.explodingkittens.View.client;

import group15.explodingkittens.Controller.ClientServer.Client.Client;
import group15.explodingkittens.View.client.ClientView;
import group15.explodingkittens.Model.exceptions.ExitProgram;
import group15.explodingkittens.Model.exceptions.ServerUnavailableException;
import group15.explodingkittens.Controller.ClientServer.ProtocolMessages;

import java.net.InetAddress;
import java.util.Scanner;

public class ClientTUI implements ClientView {
    private Client gameClient;
    private Scanner scanner;

    public ClientTUI(Client gameClient){
        this.gameClient = gameClient;
        this.scanner = new Scanner(System.in);
    }


    @Override
    public void start() {
        showMessage("Welcome to the game!");

        while(true){
            showMessage("Enter your command: ");
            String userInput = scanner.nextLine();

            try {
                handleUserInput(userInput);

            } catch (ExitProgram exitProgram) {
                showMessage("Exit TUI.");
                break;
            } catch (ServerUnavailableException exception) {
                showMessage("Error: Server Unabailable. " + exception.getMessage());
            }
        }
        scanner.close();
    }

    @Override
    public void handleUserInput(String input) throws ExitProgram, ServerUnavailableException {
        String[] splitInput = input.split(" ");
        String command = splitInput[0];

        switch(command){
            case ProtocolMessages.PlayCard:
                showMessage("Play card received");
                break;
            case ProtocolMessages.CheckDeckSize:
                showMessage("Check deck size received");
                break;
            case ProtocolMessages.EndTurn:
                showMessage("End turn received");
                break;
            case ProtocolMessages.PlayCombo:
                showMessage("Play combo received");
                break;
            case ProtocolMessages.DoExplode:
                showMessage("Explode received");
                break;
            case ProtocolMessages.InsertExplode:
                showMessage("Insert explode received");
                break;
            case ProtocolMessages.RequestNope:
                showMessage("Nope request received");
                break;
            case ProtocolMessages.ChooseFavor:
                showMessage("Chosen favor received");
                break;
            case ProtocolMessages.SendMessage:
                showMessage("Message received");
                break;
            case ProtocolMessages.DoForwardMessage:
                showMessage("Forward message received");
                break;
            case ProtocolMessages.DoGeneralAnnounce:
                showMessage("General announcement received");
                break;
            case ProtocolMessages.DoPrivateAnnounce:
                showMessage("Private announcement received");
                break;
            case ProtocolMessages.DoErrorDisplay:
//                showMessage("Error received");
                break;
            case ProtocolMessages.DoStatusDisplay:
//                showMessage("Status display received");
                break;
            case "help":
                printHelpMenu();
                break;
            default:
                showMessage("Invalid command. Type 'help' to see the help menu");
        }
    }

    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }

    @Override
    public InetAddress getIp() {
        showMessage("Enter IP address: ");
        try{
            return InetAddress.getByName(scanner.nextLine());
        }catch(Exception exception){
            showMessage("Invalid address. Try again!");
            return getIp();
        }
    }

    @Override
    public String getString(String question) {
        showMessage(question);
        return scanner.nextLine();
    }

    @Override
    public int getInt(String question) {
        showMessage(question);
        try{
            return Integer.parseInt(scanner.nextLine());
        }catch(NumberFormatException exception){
            showMessage("Invalid input. Try again using an integer!");
            return getInt(question);
        }
    }

    @Override
    public boolean getBoolean(String question) {
        showMessage(question + "yes or no");
        String input = scanner.nextLine().toLowerCase();
        return input.equals("yes");
    }

    @Override
    public void printHelpMenu() {
        showMessage("Commands: ");
        showMessage("PC - play card");
        showMessage("CDS - check deck size");
        showMessage("ET - end turn");
        showMessage("PCO - play combo");
        showMessage("DX - explode");
        showMessage("IE - insert explode");
        showMessage("RN - request nope");
        showMessage("CF - choose favor");
        showMessage("SM - send message");
        showMessage("DFM - do forward message");
        showMessage("DGA - do general announcement");
        showMessage("DPA - do private announcement");
        showMessage("DED - do error display");
        showMessage("DsD - do status display");
    }
}
