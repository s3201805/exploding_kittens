package group15.explodingkittens.Controller.ClientServer;

import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Player.Player;

import java.util.InputMismatchException;
import java.util.Scanner;



public class GameLogic {
    public Game getGame() {
        return game;
    }

    private Game game;


    public GameLogic() {
    }
    /**
     * Handles input of commands and changes state of game.
     */
    public void handleInput(String command){
        System.out.println(command);
        System.out.println(game.getCurrentPlayer().getCardsInHand());
        String[] commands = command.split(" ");


        }

    /**
     * Starts a game
     */
    public void start() {

        while (game.isContinueGame()) {
            game.reset();

            game.getDeck().shuffle();

            play();

            boolean validInput = false;

            while (!validInput) {
                System.out.println("\n> Play another time? (true/false)?");


                try {
                    Scanner scanner = new Scanner(System.in);
                    game.setContinueGame(Boolean.parseBoolean(scanner.nextLine()));
                    validInput = true;
                } catch (InputMismatchException e) {
                    System.out.println("Invalid input. Please enter 'true' or 'false'.");
                }
            }


        }

    }

    public void play() {
        while (!game.gameOver()) {
            Player currentPlayer = game.getCurrentPlayer(); //Get current player
            System.out.println("It's " + currentPlayer.getName() + "'s turn!");

            if (!currentPlayer.isTurnSkipped()) { //If they are not skipped by previous player, continue

                System.out.println(currentPlayer.getName() + ": it is your turn! " + currentPlayer.handToString());

                Move move = currentPlayer.determineMove(); //Decide on what move to do
                currentPlayer.processMove(move); //Process this move

            } else { //Current player is skipped
                System.out.println("Sorry, you are skipped :(");
                currentPlayer.endTurn();
            }

            if (game.gameOver()) {
                game.printResult();
                break;
            }

            game.setCurrent(game.getCurrent() + 1);
            if (game.getCurrent() == game.NUMBER_PLAYERS) {
                game.setCurrent(0);
            }

            game.update(); //Shows the deck size

        }
    }
}
