package group15.explodingkittens.Controller.ClientServer.Client;

import java.io.*;
import java.net.Socket;

public class Client implements Runnable {
//    private Socket clientSocket;
//    private BufferedReader in;
//    private BufferedWriter out;

//    private ClientTUI tui;

//    public Client(Socket clientSocket, String username) {
//        try {
//            this.clientSocket = clientSocket;
//            this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
//            this.out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
//            this.username = username;
//            this.tui = new ClientTUI(this);
////            this.handler = new ClientHandler(clientSocket, new Server(12345), "Client");
//            tui.start();
////            handler.run();
//
//        } catch (IOException exception) {
//            closeEverything(clientSocket, in, out);
//        }
//    }

//    MH;SOPHIE;human_player
//    MH;CLARA;human_player
    private String username;

    public Client() {

    }
//
//    public void clearConnection() {
//        clientSocket = null;
//    }
//
//    public void sendMessageTo() {
//        try {
//             out.write(username);
//             out.newLine();
//             out.flush();
//
//             Scanner scanner = new Scanner(System.in);
//             while(clientSocket.isConnected()){
//                 String messageToSend = scanner.nextLine();
//                 out.write(username + ": "+ messageToSend);
//                 out.newLine();
//                 out.flush();
//             }
//        } catch (IOException exception) {
//            closeEverything(clientSocket, in, out);
//        }
//    }
//
    public void closeEverything(Socket playerSocket, BufferedReader bufferedReader, BufferedWriter bufferedWriter){
        try{
            if(bufferedReader != null){
                bufferedReader.close();
            }
            if(bufferedWriter != null){
                bufferedWriter.close();
            }
            if(playerSocket != null){
                playerSocket.close();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

//    public void listenForMessages() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String messageFromGroupChat;
//                while(clientSocket.isConnected()){
//                    try{
//                        messageFromGroupChat = in.readLine();
//                        System.out.println(messageFromGroupChat);
//                    }catch(IOException e){
//                        closeEverything(clientSocket, in, out);
//                    }
//                }
//            }
//        }).start();
//
//    }

//    public void closeConnection() {
//        System.out.println("Closing the connection...");
//        try {
//            in.close();
//            out.close();
//            clientSocket.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
    @Override
    public void run() {
        System.out.println("[SERVER]: Hello and welcome! To participate: please type in 'MH;[your name];human_player' if you want to participate. \n");
//                "[SERVER]: if this should be a computer player, please type in 'ACP;[computer name]' \n " +
//                "[SERVER]: To start the game, please enter 'SG'" +
//                "[SERVER]: Have fun!");
        try {
            Socket socket = new Socket("localhost", 4000);
            ServerHandler handler = new ServerHandler(socket);
            new Thread(handler).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
//        try{
//          Scanner scanner = new Scanner(System.in);
//            System.out.println("Enter your username for the group chat: ");
//            String username = scanner.nextLine();
//            Socket clientSocket = new Socket("localhost", 1234);
//            Client client = new Client(clientSocket, username);
//            client.listenForMessages();
//            client.sendMessageTo();
//        }catch(Exception exception){
//            System.out.println("Could not connect due to incorrect details.");
//        }

        Client client = new Client();
        client.run();
    }


}
