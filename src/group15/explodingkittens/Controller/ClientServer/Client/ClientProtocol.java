package group15.explodingkittens.Controller.ClientServer.Client;


import java.util.ArrayList;

/**
 * @author Matthijs Jansen op de Haar
 * Client protocol, functions are defined in the same order of the document
 * See google docs for extra info: https://docs.google.com/document/d/1cOYHhskek6N5XCR120pK11z8LlXu0yHKpjaEDSVdX5E/edit?usp=sharing
 */

public interface ClientProtocol {
    //Constants, that were settled on. To split the messages.
    char MAIN_SPLITTER = ';';
    char CHILD_SPLITTER = '-';
    char GRANDCHILD_SPLITTER = '#';
    //Standard return type == String

    // FUNCTIONS

    String chooseFavor(int chosenCard);

    // TURN ACTIONS

    /**
     * Indicates server to play a card, optionally includes a playerIndex if not needed [index == -1]
     * @param cardIndex - Refers to the index of the card position in hand
     * @param playerIndex - Refers to the index of the player in the game
     * @return message
     * @ensures That index is within bounds
     */
    String PlayCard(int cardIndex, int playerIndex);

    /**
     * Indicates server that player wants to check decksize
     * @return message
     */
    String checkDeckSize();

    /**
     * Indicates server that player wants to end turn
     * @return message
     */
    String EndTurn();

    /**
     * Indicates to server that player wants to play a combo.
     * @param selectedCards - Indicates the cards from the hand that should be played via an index
     * @param playerIndex - Indicates the affected player by index
     * @return message
     * @ensures that indexes are in bounds
     * @ensures that selectedCards are of the same type
     * @ensures that selectedCards if of size 2 or 3
     */
    String PlayCombo(ArrayList<Integer> selectedCards, int playerIndex);

    /**
     * Explodes the player on the client
     * @return message
     */
    String doExplode();

    /**
     * Indicates the server where an exploding ticket should be placed after defusing
     * @param deckIndex - index of the spot the card should be inserted
     * @return message
     * @ensures that deck index is in bounds
     */
    String insertExplode(int deckIndex);

    /**
     * Reply from the client if nope card should be used or not
     * @param isUsed - true means used. false means not used
     * @return message
     */
    String requestNopeResponse(boolean isUsed);



    // LOBBY

    /**
     * Indicates to the server that a lobby should be created.
     * @param size - Indicates size of the lobby
     * @param amountOfBots - Indicates the amount of spots filled by bots
     * @param gameMode - CURRENTLY WIP, BUT INDEX WOULD REFER TO DIFFERENT GAME MODES
     * @return message
     * @ensures size is between 2 and 5
     * @ensures amountOfBots is between 0 and size - 1
     * @ensures gameMode is not out of bounds
     */
    String createLobby(int size, int amountOfBots, int gameMode);

    /**
     * Indicates to server that lobby should be deleted. Server takes the player that called the function.
     * @return message
     */
    String deleteLobby();

    /**
     * Responds to the server's inquiry if player wants to play another game
     * @param answer - refers to the response from the client, true means they want to continue. False means they do not want to
     * @return message
     */
    String respondReplayGame(boolean answer);

    /**
     * Asks the server for the current lobbies
     * @return message
     */
    String requestCurrentLobbies();

    /**
     * Asks server to join a certain lobby
     * @param player - refers to the host player of the lobby, as the index may change in the list.
     * @return
     * @ensures no lobby has been joined and the host-player exists
     */
    String joinLobby(String player);

    /**
     * Indicates to the server that user wants to leave the lobby.
     * @param player - Indicates the host player
     * @return message
     */
    String leaveLobby(String player);

    /**
     * Indicates to server that game will start
     * @return message
     */
    String startGame();

    /**
     * Indicates to server that the client wants to leave the game
     * @return message
     */
    String leaveGame();

    /**
     * Allows the client to select a name to associate with their player.
     * @param name - corresponds to the name the player wants to use
     * @return message
     * @ensures that name has not been taken
     */
    String selectName(String name);




    // CHAT

    /**
     * Sends message to a specific lobby or its associated game
     * @param message - refers to the contents of the message
     * @param HostPlayer - refers to the host player of the game
     * @return message
     * @ensures that the contents of the message are accepted in the JVM
     */
    String sendMessage(String message, String HostPlayer);

    /**
     * Displays any received messages
     * @return message
     */
    String doForwardMessage();




    // ANNOUNCEMENTS

    /**
     * Displays any announcements from the server
     * @return message
     */
    String doGeneralAnnouncement();

    /**
     * Displays any private announcements from the server
     * @return message
     */
    String doPrivateAnnouncement();

    /**
     * Displays error messages
     * @return message
     */
    String doErrorDisplay();

    /**
     * Displays status messages
     * @return message
     */
    String doStatusDisplay();
}
