package group15.explodingkittens.Controller.ClientServer.Client;

import group15.explodingkittens.Controller.ClientServer.ProtocolMessages;


import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class ServerHandler implements Runnable, ClientProtocol {
    private BufferedWriter out;
    private BufferedReader in;
    private Socket socket;

    public ServerHandler(Socket socket) throws IOException {

        this.socket = socket;

//        try{
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
//        } catch (IOException e){
//            closeEverything(socket, in, out);
//        }
    }

    @Override
    public void run() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (socket.isConnected()) {
                    //Interprets commands entered by the user
                    String msg = scanner.nextLine();
                    String[] commands = msg.split(";");

                    switch (commands[0]) {
                        case "MH":
                            if(commands.length!=3){
                                System.out.println("[SERVER]: Invalid input.");
                                break;
                            }
                            doMakeHandshake(commands[1], commands[2]);
                            break;
                        case ProtocolMessages.PlayCard:
                            if (commands.length != 3) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid card and player integer.");
                                break;
                            }
                            PlayCard(Integer.parseInt(commands[1]), Integer.parseInt(commands[2]));
                            break;
                        case ProtocolMessages.CheckDeckSize:
                            checkDeckSize();
                            break;
                        case ProtocolMessages.EndTurn:
                            EndTurn();
                            break;
                        case ProtocolMessages.PlayCombo:
                            if (commands.length !=3 ) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid card combo and player integer.");
                                break;
                            }

                            ArrayList<Integer> ints = new ArrayList<>();

                            for (String cardInt : commands[1].split("-")){
                                ints.add(Integer.parseInt(cardInt));
                            }
                            PlayCombo(ints, Integer.parseInt(commands[2]));
                            break;
                        case ProtocolMessages.DoExplode:
                            doExplode();
                            break;
                        case ProtocolMessages.InsertExplode:
                            if (commands.length != 2) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid integer.");
                                break;
                            }
                            insertExplode(Integer.parseInt(commands[1]));
                            break;
                        case ProtocolMessages.RequestNopeResponse:
                            if (commands.length != 2) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid boolean.");
                                break;
                            }
                            requestNopeResponse(Boolean.parseBoolean(commands[1]));
                            break;
                        case ProtocolMessages.ChooseFavor:
                            if (commands.length != 2) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid integer.");
                                break;
                            }
                            chooseFavor(Integer.parseInt(commands[1]));
                            break;
                        case ProtocolMessages.StartGame:
                            if (commands.length != 1) {
                                System.out.println("[SERVER]: Invalid input. Only 'SG' to start a game.");
                                break;
                            }
                            startGame();
                            break;
                        case ProtocolMessages.LeaveGame:
                            if (commands.length != 1) {
                                System.out.println("[SERVER]: Invalid input. Only 'LG' to leave the game.");
                                break;
                            }
                            leaveGame();
                            break;
                        case ProtocolMessages.SelectName:
                            if (commands.length != 2) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid name.");
                                doMakeHandshake(commands[1], commands[2]);
                                break;
                            }
                            selectName(commands[1]);
                            break;
                        case ProtocolMessages.SendMessage:
                            if (commands.length != 3) {
                                System.out.println("[SERVER]: Invalid input. Please choose a valid message and host player.");
                                break;
                            }
                            sendMessage(commands[1], commands[2]);
//                            doForwardMessage();
                            break;
                        case ProtocolMessages.DoForwardMessage:
                            doForwardMessage();
                            break;
                        case ProtocolMessages.DoGeneralAnnounce:
                            doGeneralAnnouncement();
                            break;
                        case ProtocolMessages.DoPrivateAnnounce:
                            doPrivateAnnouncement();
                            break;
                        case ProtocolMessages.DoErrorDisplay:
                            doErrorDisplay();
                            break;
                        case ProtocolMessages.DoStatusDisplay:
                            doStatusDisplay();
                            break;
                    }
                }
            }
        }).start();
        try {
            String msgFromServer = "";
            while (socket.isConnected()) {
                //handles server responses
                msgFromServer = in.readLine();
                String[] commands = msgFromServer.split(";");
                try {
                    switch (commands[0]){
                        case "AH":
                            handleAcceptHandshake();
                            break;
                        case ProtocolMessages.RequestAction:
                            handleRequestAction(commands[1]);
                            break;
                        case ProtocolMessages.HandlePlayCard:
                            System.out.println("[SERVER]: " + commands[1] + "played " + commands[2]);
                            break;
                        case ProtocolMessages.ShowDeckSize:
                            System.out.println("[SERVER]: Deck size: " + commands[1]);
                            break;
                        case ProtocolMessages.HandleEndTurn:
                            System.out.println("[SERVER]: Player " + commands[1] + " ended their turn.");
                            break;
                        case ProtocolMessages.HandlePlayCombo:
                            System.out.println("[SERVER]: Player " + commands[1] + " played a combo.");
                            break;
                        case ProtocolMessages.ExplodePlayer:
                            System.out.println("[SERVER]: Player " + commands[1] + " blew up.");
                            break;
                        case ProtocolMessages.HandleInsert:
                            System.out.println("[SERVER]: Exploding kitten is inserted.");
                            break;
                        case ProtocolMessages.RequestNope:
                            System.out.println("[SERVER]: Nope requested.");
                            break;
                        case ProtocolMessages.HandleRequestNope:
                            System.out.println("[SERVER]: Nope request handled.");
                            break;
                        case ProtocolMessages.RequestFavor:
                            System.out.println("[SERVER]: Request favor was a success.");
                            break;
                        case ProtocolMessages.HandleRequestFavor:
                            System.out.println("[SERVER]: Request favor handled.");
                            break;
                        case ProtocolMessages.HandleStartGame:
//                            handleRequestAction(commands[1]);
                            handleGameInfo(commands[1], commands[2], commands[3], commands[4]);
                            System.out.println("[SERVER]: Game has started.");
                            break;
                        case ProtocolMessages.HandleLeaveGame:
                            System.out.println("[SERVER]: Player has left successfully.");
                            break;
                        case ProtocolMessages.RequestName:
                            System.out.println("[SERVER]: Request name was a success.");
                            break;
                        case ProtocolMessages.HandleSelectName:
                            System.out.println("[SERVER]: Select name was a success.");
                            break;
                        case ProtocolMessages.DoForwardMessage:
                            System.out.println("[SERVER]: Forwarded message: '" + commands[1] + "' from player " + commands[2]);
                            break;
                        case ProtocolMessages.AnnounceInGeneral:
                            System.out.println("[SERVER]: Announce in general was a success.");
                            break;
                        case ProtocolMessages.AnnounceInPrivate:
                            System.out.println("[SERVER]: Announce in private was a success.");
                            break;
                        case ProtocolMessages.DisplayErrorCode:
                            System.out.println("[SERVER]: Display error code was a success.");
                            break;
                        case ProtocolMessages.DisplayStatusCode:
                            System.out.println("[SERVER]: Display status code was a success.");
                            break;


                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleGameInfo(String deck, String hand, String playersList, String result) {
        System.out.println("[SERVER]: Deck size is: " + deck + "\n[SERVER]:" + hand + "\n[SERVER]:" + playersList + "\n[SERVER]: Is it your turn? - " + result);
    }


    private void handleRequestAction(String currentPlayer) {
        System.out.println("[SERVER]: " + currentPlayer + ": It is your turn! Please insert what action you want to play.\n" +
                "PC;card index from hand;player index (play card)\n" +
                "CDS (check deck size)\n" +
                "ET (end turn and draw a card)\n" +
                "PCO; selected cards of same type, ints split by a dash '-';player index; card type (lowercase, no spaces)\n");
    }

    private void handleAcceptHandshake() {
        System.out.println("[SERVER]: Successful handshake :)");
        broadcastMessage("[SERVER]: Successful handshake :)");
    }

    public void broadcastMessage(String message){
        try{
            out.write(message);
            out.newLine();
            out.flush();
        }catch(IOException e){
            e.printStackTrace();
        }
    }



    public void doMakeHandshake(String name, String type) {
        broadcastMessage("MH" + ";" + name + ";" + type);
    }
    @Override
    public String chooseFavor(int chosenCard) {
        return null;
    }

    @Override
    public String PlayCard(int cardIndex, int playerIndex) {
        broadcastMessage(ProtocolMessages.HandlePlayCard + ";" + cardIndex + ";" + playerIndex);
        return null;
    }

    @Override
    public String checkDeckSize() {
        broadcastMessage(ProtocolMessages.CheckDeckSize);
        return ProtocolMessages.CheckDeckSize;
    }

    @Override
    public String EndTurn() {
        broadcastMessage(ProtocolMessages.EndTurn);
        return null;
    }

    @Override
    public String PlayCombo(ArrayList<Integer> selectedCards, int playerIndex) {
        return null;
    }

    @Override
    public String doExplode() {
        return null;
    }

    @Override
    public String insertExplode(int deckIndex) {
        return null;
    }

    @Override
    public String requestNopeResponse(boolean isUsed) {
        return null;
    }

    @Override
    public String createLobby(int size, int amountOfBots, int gameMode) {
        return null;
    }

    @Override
    public String deleteLobby() {
        return null;
    }

    @Override
    public String respondReplayGame(boolean answer) {
        return null;
    }

    @Override
    public String requestCurrentLobbies() {
        return null;
    }

    @Override
    public String joinLobby(String player) {
        return null;
    }

    @Override
    public String leaveLobby(String player) {
        return null;
    }

    @Override
    public String startGame() {
        broadcastMessage(ProtocolMessages.StartGame);
        return null;
    }

    @Override
    public String leaveGame() {
        return null;
    }

    @Override
    public String selectName(String name) {
        return null;
    }

    @Override
    public String sendMessage(String message, String HostPlayer) {
        broadcastMessage(ProtocolMessages.SendMessage + ";" + message + ";" + HostPlayer);

        return null;
    }

    @Override
    public String doForwardMessage() {
        broadcastMessage(ProtocolMessages.SendMessage);
        return null;
    }

    @Override
    public String doGeneralAnnouncement() {
        return null;
    }

    @Override
    public String doPrivateAnnouncement() {
        return null;
    }

    @Override
    public String doErrorDisplay() {
        return null;
    }

    @Override
    public String doStatusDisplay() {
        return null;
    }

    public void closeEverything(Socket playerSocket, BufferedReader bufferedReader, BufferedWriter bufferedWriter){
        try{
            if(bufferedReader != null){
                bufferedReader.close();
            }
            if(bufferedWriter != null){
                bufferedWriter.close();
            }
            if(playerSocket != null){
                playerSocket.close();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
