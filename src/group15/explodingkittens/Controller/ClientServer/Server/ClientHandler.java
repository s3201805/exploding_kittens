package group15.explodingkittens.Controller.ClientServer.Server;

import group15.explodingkittens.Model.Card.Card;
import group15.explodingkittens.Model.Card.CardCategory;
import group15.explodingkittens.Model.Player.HumanPlayer;
import group15.explodingkittens.Model.Player.ComputerPlayer;
import group15.explodingkittens.Model.Move.Move;
import group15.explodingkittens.Model.Move.MoveType;
import group15.explodingkittens.Model.Player.Player;
import group15.explodingkittens.Controller.ClientServer.ProtocolMessages;
//import jdk.internal.jimage.BasicImageReader;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class ClientHandler implements Runnable, ServerProtocol {
    public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private BufferedWriter out;
    private BufferedReader in;
    private Socket playerSocket;
    private String playerUsername;
    private Server srv;
//    protected boolean inGame = false;
    public Socket getPlayerSocket(){
        return this.playerSocket;
    }
    public String getPlayerUsername(){
        return this.getClientUsername();
    }


    public ClientHandler(Socket playerSocket, Server srv) throws IOException {
        try{
            this.playerSocket = playerSocket;
            this.out = new BufferedWriter(new OutputStreamWriter(playerSocket.getOutputStream()));
            this.in = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));
            this.srv = srv;
            //this.playerUsername = bufferedReader.readLine();
            clientHandlers.add(this);
            //broadcastMessage("SERVER: " + playerUsername + " has entered the chat!");
        }catch (IOException e){
            e.printStackTrace();
            closeEverything(playerSocket, in, out);
        }

    }

    @Override
    public void run() {

        try {
            while (playerSocket.isConnected()) {
                String messageFromClient;
                messageFromClient = this.in.readLine();
                String[] commands = messageFromClient.split(";");

                broadcastMessage(messageFromClient);
//               int nopeCount = 0;
                switch(commands[0]){
//                    case "MM":
//                        System.out.println("I am asked to make a move");
//                        break;
//                    case "NOPE":
//                        for(ClientHandler ch : srv.getClients()){
//                            ch.bufferedWriter.write("Do you want to play a nope?");
//                            String answer = ch.bufferedReader.readLine();
//                            if(answer.equals("yes")){
//                                nopeCount++;
//                            }
//                        }
//                        if(nopeCount % 2 == 1){
//                            this.bufferedWriter.write("Your action was noped!");
//                            break;
//                        }
                    case ProtocolMessages.SelectName:
                        if (commands.length != 3) {
                            broadcastMessage("Please input a name in the following format " +
                                    "SN;<Name>;human_player or SN;<Name>;computer_player");
                        }

                            break;
                    case "MH":
                        this.playerUsername = commands[1];
                        handleHandshake(commands[1], commands[2]);
                        break;
                    case "ACP":
                        handleCPU(commands[1]);
                        break;
                    case ProtocolMessages.StartGame:
                        ArrayList<String> players = new ArrayList<>();
                        for (Player player : srv.getGame().getActivePlayers()) {
                            players.add(player.getName());
                        }
                        handleStartGame(players, null);
                        break;
                    case ProtocolMessages.SendMessage:
                        forwardMessage(commands[2], commands[1]);
                        break;
                    case ProtocolMessages.LeaveGame:
                        handleLeaveGame();
                        break;
                    case ProtocolMessages.PlayCard:
                        handlePlayCard();
                        break;
                    case ProtocolMessages.CheckDeckSize:
                        int deckSize = srv.getGame().getDeck().getSize();
                        showDeckSize(deckSize, 1);
                        break;
                    case ProtocolMessages.EndTurn:
                        int current = srv.getGame().getCurrent();
                        CardCategory drawnCard = srv.getGame().getActivePlayers().get(current).getDrawnCard();
                        handleEndTurn(drawnCard);
                        break;
                    case ProtocolMessages.PlayCombo:
                        handlePlayCombo();
                        broadcastMessage(handlePlayCombo());
                        break;
                    case ProtocolMessages.DoExplode:
                        int currentInt = srv.getGame().getCurrent();
                        Player currentPlayer = srv.getGame().getActivePlayers().get(currentInt);
                        explodePlayer(currentPlayer.isExploded(), srv.getGame().getDeck().getSize());
                        break;
                    case ProtocolMessages.InsertExplode:
                        handleInsert();
                        broadcastMessage(handleInsert());
                        break;
                    case ProtocolMessages.RequestNope:
                        handleRequestNope();
                        broadcastMessage(handleRequestNope());
                        break;
                    case ProtocolMessages.ChooseFavor:
                        ArrayList<CardCategory> cardsF = new ArrayList<>();
                        for (Card card : srv.getGame().getActivePlayers().get(srv.getGame().getCurrent()).getCardsInHand()) {
                            cardsF.add(card.getCardType());
                        }
                        requestFavor(cardsF);
                        handleRequestFavor();
                        broadcastMessage(handleRequestFavor());
                        break;
                    case ProtocolMessages.DoForwardMessage:
                        if (commands.length != 3) {
                            broadcastMessage("Please input a command in the following format " +
                                    "DFM;<Name sending player>;message");
                        }
                        forwardMessage(commands[1], commands[2]);
                        broadcastMessage(forwardMessage(commands[1], commands[2]));
                        break;
                    case ProtocolMessages.DoGeneralAnnounce:
                        if (commands.length != 2) {
                            broadcastMessage("Please input a command in the following format " +
                                    "DGA;message");
                        }
                        announceInGeneral(commands[1]);
                        break;
                    case ProtocolMessages.DoPrivateAnnounce:
                        if (commands.length != 3) {
                            broadcastMessage("Please input a command in the following format " +
                                    "DPA;message");
                        }
                        announceInPrivate(commands[1]);
                        break;
                    case ProtocolMessages.DoErrorDisplay:
                        if (commands.length != 3) {
                            broadcastMessage("Please input a command in the following format " +
                                    "DED;code;message");
                        }
                        displayErrorCode(commands[1], commands[2]);
                        break;
                    case ProtocolMessages.DoStatusDisplay:
                        if (commands.length != 3) {
                            broadcastMessage("Please input a command in the following format " +
                                    "DSD;code;message");
                        }
                        displayStatusCode(commands[1], commands[2]);
                        break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeEverything(playerSocket, in, out);
        }
    }

    public void broadcastMessage(String messageToSend){
        for(ClientHandler clientHandler : clientHandlers){
            try{
                clientHandler.out.write(messageToSend);
                clientHandler.out.newLine();
                clientHandler.out.flush();
            }catch(IOException e){
                closeEverything(playerSocket, in, out);
            }
        }

    }

    public void removeClientHandler(){
        clientHandlers.remove(this);
        broadcastMessage("[SERVER]: "+ playerUsername + " has left the chat!");
    }

    public void closeEverything(Socket playerSocket, BufferedReader bufferedReader, BufferedWriter bufferedWriter){
        removeClientHandler();
        try{
            if(bufferedReader != null){
                bufferedReader.close();
            }
            if(bufferedWriter != null){
                bufferedWriter.close();
            }
            if(playerSocket != null){
                playerSocket.close();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public String getClientUsername() {
        return playerUsername;
    }

    public void setClientUsername(String playerUsername) {
        this.playerUsername = playerUsername;
    }

    public void handleHandshake(String name, String type) {
        doAcceptHandshake();
//        if (!srv.getGame().isContinueGame()) {
            if (type.equals("human_player")) {
                srv.getGame().getActivePlayers().add(new HumanPlayer(name, new ArrayList<>(), srv.getGame().getDeck(), srv.getGame()));
            } else {
                srv.getGame().getActivePlayers().add(new ComputerPlayer(name, new ArrayList<>(), srv.getGame().getDeck(), srv.getGame()));
            }
//        }
    }

    private void doAcceptHandshake() {
        broadcastMessage("AH");
    }

    public void handleCPU(String name) {
        srv.getGame().getActivePlayers().add(new ComputerPlayer(name, new ArrayList<>(), srv.getGame().getDeck(), this.srv.getGame()));
    }

    @Override
    public String requestFavor(List<CardCategory> playableCards) {
        int current = srv.getGame().getCurrent();
        return "Please pick a card to play: " + srv.getGame().getActivePlayers().get(current).handToString();
    }

    @Override
    public String handleRequestFavor() {
        return null;
    }

    @Override
    public String requestAction(ArrayList<CardCategory> playableCards, ArrayList<String> alivePlayers) {
        int current = srv.getGame().getCurrent();
        String playerHand = srv.getGame().getActivePlayers().get(current).handToString();
        String availablePlayers = srv.getGame().getActivePlayers().toString();

        return "Please pick an action to play: " + playerHand + "\n" + "Available players: " + availablePlayers;    }

    @Override
    public String handlePlayCard() {
        Player currentPlayer = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
        Move move = new Move(MoveType.PLAY, currentPlayer);
        currentPlayer.processMove(move);
        broadcastMessage(ProtocolMessages.HandlePlayCard);

        return "Processing card play...";
    }

    @Override
    public String showDeckSize(int deckSize, int explodingKittens) {
        broadcastMessage("SDS;" + String.valueOf(deckSize));
        return "Deck size: " + deckSize;

    }

    @Override
    public String handleEndTurn(CardCategory drawnCard) {
        Player currentPlayer = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
        currentPlayer.drawCard(srv.getGame().getDeck());
        broadcastMessage(ProtocolMessages.SendMessage + ";turn is ended");

        return "Processing ending turn and drawing a card...";
    }

    @Override
    public String handlePlayCombo() {
        Player currentPlayer = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
//        Move move = new Move(MoveType.TWO, chooseNCards(2), currentPlayer);
        return null;
    }

    @Override
    public String explodePlayer(boolean isExploded, int deckSize) {
        Player currentPlayer = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
        currentPlayer.blowsUp();
        return "Processing blow up...";
    }

    @Override
    public String handleInsert() {
        Player currentPlayer = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
        System.out.println("You blew up but had an exploding kitten, please insert an integer to reinsert the kitten.");

        System.out.println("You pulled an exploding kitten, but had a defuse card :). Please return the card in the " +
                "pile, please provide an index to return into the draw pile.");

        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();

        srv.getGame().getDeck().getCardsDeck().add(index, new Card(currentPlayer.getDrawnCard()));
        currentPlayer.getCardsInHand().removeIf(card -> card.getCardType() == CardCategory.DEFUSE);

        return null;
    }

    @Override
    public String requestNope() {
        System.out.println("Do you want to play a nope card?");
        Scanner scanner = new Scanner(System.in);

        if (scanner.nextBoolean()) {
            handleRequestNope();
            return "Processing nope...";
        } else {
            return null;
        }
    }

    @Override
    public String handleRequestNope() {
        return null;
    }

    @Override
    public String handleCreateLobby() {
        return null;
    }

    @Override
    public String handleDeleteLobby() {
        return null;
    }

    @Override
    public String askReplayGame() {
        return null;
    }

    @Override
    public String handleReplayResponse(boolean isNewGame) {
        return null;
    }

    @Override
    public String returnLobbyList(HashMap<String, ArrayList<Integer>> lobbyList) {
        return null;
    }

    @Override
    public String handleJoinLobby() {
        return null;
    }

    @Override
    public String handleLeaveLobby() {
        return null;
    }

    @Override
    public String updateEnteredLobby(String playerName, boolean isJoining) {
        return null;
    }

    @Override
    public String handleStartGame(ArrayList<String> turnOrder, ArrayList<CardCategory> hand) {
//        String order = "Player 1 (index 0): " + game.getActivePlayers().get(0).getName() +
//                "Player 2 (index 1): " + game.getActivePlayers().get(1).getName();
//
//        Player thisPlayer = clients
//        String individualHand = game.getActivePlayers().get(game.getCurrent()).handToString();
//        if (srv.getGame().getActivePlayers().size() == 2) {

//            srv.getGame().start();
//            printGameInfo(srv.getGame().getCurrentPlayer().getName());
//            broadcastMessage(ProtocolMessages.HandleStartGame + ";" + srv.getGame().getCurrentPlayer().getName()+ ";" + srv.getGame().getDeck().getSize()+ ";no");
//        }
        doGameInfo();
        return "Game is being started. The players are: " + turnOrder;
    }

    public void doGameInfo() {
        for(ClientHandler clientHandler : srv.getClientHandlers()){
            String deck = "" + srv.getGame().getDeck().getSize();
            StringBuilder hand = new StringBuilder();
            String result = "no";
            StringBuilder players = new StringBuilder("These players are currently in the game: ");

            for(Player player : srv.getGame().getActivePlayers()) {


            players.append(", ").append(player.getName()).append("-").append(player.getCardsInHand().size());


                    if(clientHandler.getClientUsername().equals(player.getName())){
                        hand.append(player.getCardsInHand());
                    }
                    if(srv.getGame().getCurrentPlayer().getName().equals(clientHandler.getClientUsername())){
                        result = "yes";
                    }

                }
            String message = ProtocolMessages.HandleStartGame + ";"+deck+";"+hand+";"+players+";"+result;
            clientHandler.broadcastMessage(message);
        }
    }
    @Override
    public String handleLeaveGame() {
        Player player = srv.getGame().getActivePlayers().get(srv.getGame().getCurrent());
        srv.getGame().getActivePlayers().remove(player);
        closeEverything(playerSocket, in, out);

        return null;
    }

    @Override
    public String requestName() {
        return null;
    }

    @Override
    public String handleSelectName() {
        return null;
    }

    @Override
    public String forwardMessage(String sendingPlayer, String message) {
        for (ClientHandler clientHandler : clientHandlers) {
            broadcastMessage(ProtocolMessages.DoForwardMessage + ";" + sendingPlayer + ";" + message);
//            broadcastMessage(ProtocolMessages.RequestAction);
        }
        return null;
    }

    @Override
    public String announceInGeneral(String message) {
        for (ClientHandler clientHandler : srv.getClientHandlers()) {
            clientHandler.broadcastMessage(message);
        }

        return "Message sent to every player";
    }

    @Override
    public String announceInPrivate(String message) {
        return null;
    }

    @Override
    public String displayErrorCode(String code, String message) {
        return null;
    }

    @Override
    public String displayStatusCode(String code, String message) {
        return null;
    }
}
