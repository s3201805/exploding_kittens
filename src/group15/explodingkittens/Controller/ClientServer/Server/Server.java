package group15.explodingkittens.Controller.ClientServer.Server;

import group15.explodingkittens.Model.Game.Game;
import group15.explodingkittens.Model.Deck.Deck;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {
    private ServerSocket ekServerSocket;
    private ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
    private int next_client_no;
    private Game game;
    public Game getGame() {
        return this.game;
    }
    public ArrayList<ClientHandler> getClientHandlers() {
        return this.clientHandlers;
    }

//   public Server(ServerSocket ekServerSocket) {
//       this.ekServerSocket = ekServerSocket;
//   }
//
//   public Server(int humanPlayers, int computerPlayers) throws IOException {
//       this.ekServerSocket = new ServerSocket(1234);
//       this.clients = new ArrayList<>();
//       this.view = new ServerTUI();
//       this.next_client_no = 1;
//       this.game = new Game(2, 0, new Deck(2));
//   }

    public Server() {
//        this.ekServerSocket = new ServerSocket(1234);
    }

    private void closeServer() {
        try {
            if (ekServerSocket != null) {
                ekServerSocket.close();
            }
            System.out.println("Server closed");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void run(){
        System.out.println("Waiting for players to connect...");

        try{
//            while (true) {
            this.ekServerSocket = new ServerSocket(4000);
            this.game = new Game(0, 0, new Deck(2));

            while(!ekServerSocket.isClosed()){
                Socket playerSocket = ekServerSocket.accept();
                System.out.println("A player has connected!");
                ClientHandler playerClientHandler = new ClientHandler(playerSocket, this);
                clientHandlers.add(playerClientHandler);

                new Thread(playerClientHandler).start();
            }
//            }
        } catch(IOException exception) {
            exception.printStackTrace();
        } finally {
            closeServer();
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
//        server.run();
        new Thread(server).start();
    }

}
