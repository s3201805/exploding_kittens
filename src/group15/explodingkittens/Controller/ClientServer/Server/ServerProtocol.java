package group15.explodingkittens.Controller.ClientServer.Server;

import group15.explodingkittens.Model.Card.CardCategory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Matthijs Jansen op de Haar
 * Server protocol, functions are defined in the same order of the document
 * See google docs for extra info: https://docs.google.com/document/d/1cOYHhskek6N5XCR120pK11z8LlXu0yHKpjaEDSVdX5E/edit?usp=sharing
 */

public interface ServerProtocol {
    //Constants, that were settled on. To split the messages.
    char MAIN_SPLITTER = ';';
    char CHILD_SPLITTER = '-';
    char GRANDCHILD_SPLITTER = '#';
    //Standard return type == String


    //FUNCTIONS

    // New functions - Favor cards
    String requestFavor(List<CardCategory> playableCards);
    String handleRequestFavor(); // This one would simply return "HRF"

    //TURN ACTIONS

    /**
     * Shows the player a list of playable cards and requests the player to do an action (can lead to multiple responses)
     * @param playableCards - Refers to the playable cards in a players hand
     * @param alivePlayers - MAKE SURE THESE ARE STILL IN THE GAME (String refers to player's NAME)
     * @return message
     * @ensures that a player receives this message until EndTurn has been done.
     */
    String requestAction(ArrayList<CardCategory> playableCards, ArrayList<String> alivePlayers) throws IOException;

    /**
     * Handles playing a card, executes the actions, etc.
     * @return message
     */
    String handlePlayCard();

    /**
     * Shows the player the current deck size and amount of exploding kittens
     * @param deckSize - refers to the amount of cards in the DRAW PILE
     * @param explodingKittens - refers to the amount of exploding kittens still in the draw pile
     * @return message
     */
    String showDeckSize(int deckSize, int explodingKittens);

    /**
     * Handles when end turn is indicated, will return the drawn card and optionally null if no card is drawn.
     * @param drawnCard - refers to the type of the drawn card
     * @return message
     * @ensures that null is returned if a skip card was played
     */
    String handleEndTurn(CardCategory drawnCard);

    /**
     * Handles the play combo action from the player
     * @return message
     */
    String handlePlayCombo();

    /**
     * Tells the player they are exploded or not, and the deck size (for possible insertion)
     * @param isExploded - refers to if the player has exploded or not. True means they exploded. False means they had a defuse.
     * @param deckSize - refers to the size of the deck
     * @return message
     */
    String explodePlayer(boolean isExploded, int deckSize);

    /**
     * Handles the insert request from the player
     * @return message
     * @ensures that the inserted card is in the bounds of the deck
     */
    String handleInsert();

    /**
     * Asks the client if they want to use their nope card or not
     * @return message
     * @ensures that the players requested actually have a nope card
     */
    String requestNope();

    /**
     * Handles the nope request, depending on the client's response the nope is applied
     * @return message
     * @ensures that another requestNope() is done in the event a nope has been played
     */
    String handleRequestNope();



    // LOBBY

    /**
     * Handles the creation of the lobby
     * @return message
     * @ensures that the client's player profile is associated with the lobby
     */
    String handleCreateLobby();

    /**
     * Server handles the deletion of the lobby
     * @return message
     */
    String handleDeleteLobby();

    /**
     * Asks the client if they want to replay the game
     * @return message
     */
    String askReplayGame();

    /**
     * Handles the creation of a new game or not
     * @param isNewGame - refers to if there will be a new game or not. True means there will be a new one. False means that there won't be.
     * @return message
     * @ensures that no new game is made if the host leaves or all players have left.
     */
    String handleReplayResponse(boolean isNewGame);

    /**
     * Simply returns a lobby list.
     *
     * Key (String) refers to the player name
     * ArrayList<Integer> includes 1. lobbyIndex 2. lobbySize 3. currentPlayers 4. gameMode
     * All are Integer, the order is important.
     *
     * @param lobbyList - is a hashMap with all the lobbies indexed per player.
     * @param lobbyIndex - refers to the index of the lobby.
     * @param lobbySize - refers to the size of the lobby.
     * @param currentPlayers - refers to the current players in the lobby.
     * @param gameMode - CURRENTLY IRRELEVANT, BUT WOULD BE INDEXED BY GAMEMODE. FOR NOW SIMPLY USE 0/
     * @return message
     */
    String returnLobbyList(HashMap<String, ArrayList<Integer>> lobbyList);

    /**
     * Server handles the joining of players to a lobby
     * @return message
     * @ensures players are notified in Lobby
     */
    String handleJoinLobby();

    /**
     * Server handles the leaving of players to a lobby
     * @return message
     * @ensures player are notified in Lobby via updateEnteredLobby
     */
    String handleLeaveLobby();

    /**
     * Updates players of joining or leaving players
     * @param playerName - refers to the joining or leaving player
     * @param isJoining - refers to if the player is joining or not. True means they are joining. Flase means they aren't.
     * @return message
     * @ensures all current players in the lobby are updated except the one joining
     */
    String updateEnteredLobby(String playerName, boolean isJoining);


    /**
     * Handles the setup of the game, and tells every player the turn order and their hand.
     * @param turnOrder - An arraylist of strings, every string refers to a player. Is for display only.
     * @param hand - An arraylist of CardCategory, tells the client which cards they have drawn.
     * @return message
     * @ensures that every player has a defuse card and all standard rules are followed.
     * @ensures that turn order only includes every player once
     */
    String handleStartGame(ArrayList<String> turnOrder, ArrayList<CardCategory> hand) throws IOException;

    /**
     * Handles a player leaving the game
     * @return message
     * @ensures a leaving player is replaced by a CPU
     * @tip just add the cards from the player to a CPU and replace the player with a CPU player class.
     */
    String handleLeaveGame();

    /**
     * Requests name from the client
     * @return message
     */
    String requestName();

    /**
     * Handles the selection of a name, makes sure that no two names are the same
     * @return message
     * @ensures that the name is unique and does not already exist among players
     */
    String handleSelectName();



    // CHAT

    /**
     * Makes sure to forward messages from one player to the rest, also stores the chat on the server.
     * @param sendingPlayer - The name of the player who sent the message
     * @param message - the contents of the message
     * @return message
     * @ensures that the message is printable by the JVM
     */
    String forwardMessage(String sendingPlayer, String message);




    // ANNOUNCEMENTS

    /**
     * Sends an announcement to all players (usually for player actions), client handles the displaying
     * @param message - message contents
     * @return message
     */
    String announceInGeneral(String message);

    /**
     * Sends an announcement to a specific player (again, for player actions usually).
     * @param message - message contents
     * @return message
     */
    String announceInPrivate(String message);

    /**
     * Sends the client an error code along with a message
     * @param code - code is conform specifications (see doc), and refers to the error thrown
     * @param message - contains a message, is conform specifications but also include extra details
     * @return message
     * @tip you will need to develop the Exceptions, make sure to link the error codes with them.
     */
    String displayErrorCode(String code, String message);

    /**
     * Sends a status code to the user, with a certain status code
     * @param code - the code, conform specifications (see doc).
     * @param message - contains a message, is conform specifications as well but may include extra details
     * @return message
     * @ensures to try again, for example if a wrong action was specified, the server will ask for a new one.
     */
    String displayStatusCode(String code, String message);
}
