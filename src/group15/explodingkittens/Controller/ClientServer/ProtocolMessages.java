package group15.explodingkittens.Controller.ClientServer;

public class ProtocolMessages {

//    --------TURN ACTIONS----------------------------------------------------------------------------------------------
//    TURN ACTIONS
    /**
     * Server requests an action from the appropriate player. The server manages who this request gets sent to.
     * The client may respond differently. The request should be closed once the client has finished their turn.
     * (see below). The server also sends the list of playable cards for the client.
     *
     * AlivePlayers was added in the latest version, it is the viable players that can be targeted for combos and favor cards.
     */
    public static final String RequestAction = "RA";

    /**
     * A player may decide to play a card from their playable cards, in which case they return the index that
     * the card is located in their hand and optionally the player that the card affects which is -1 if not needed.
     * All players are updated (see announcements) After which the server does RequestAction again.
     */
    public static final String PlayCard = "PC";

    /**
     * Handles the above. (As every command is required to have either a Do-command from the client of Handle-command
     * from the server, these are added)
     */
    public static final String HandlePlayCard = "HPC";

    /**
     * A player may decide to check the amount of cards in the deck, and the current exploding kittens in the deck.
     */
    public static final String CheckDeckSize = "CDS";

    /**
     * The server returns the amount of cards in the deck and the amount of exploding kittens to the player. The client
     * would then handle the displaying. Happens outside the handshake. After which the server does RequestAction again.
     */
    public static final String ShowDeckSize = "SDS";

    /**
     * The player indicates to the server that they decide to end their turn.
     */
    public static final String EndTurn = "ET";

    /**
     * The server handles the ending of the turn. And returns the player the drawn card, will be null in the event a
     * skip card was played. See below in the event an exploding kitten is drawn.
     */
    public static final String HandleEndTurn = "HET";

    /**
     * (bonus feature, but handled the same for non-action cards) Players are able to select either 2 or 3 cards which
     * perform the combo actions. The targeted player also needs to be selected. After which the server does
     * RequestAction again.
     */
    public static final String PlayCombo = "PCO";

    /**
     * Handles the above.
     */
    public static final String HandlePlayCombo = "HPCO";

//  EXPLODING KITTEN IS DRAWN
    /**
     * True: Player had no defuse card and server removed them from the game including the drawn EXPLODING_KITTEN.
     * False: Player had a defuse card and was automatically played, in this case the server expects an insert from the
     * player and also includes the deck size.
     */
    public static final String ExplodePlayer = "EX";

    /**
     * Simple response from player to above.
     */
    public static final String DoExplode = "DX";

    /**
     * Player decides an index in which to insert the recently defused EXPLODING_KITTEN.
     */
    public static final String InsertExplode = "IE";

    /**
     * Server simply sends a confirmation to the player that the card was inserted, as this is not a game-wide
     * announcement, it will be contained here.
     */
    public static final String HandleInsert = "HI";

//  NOPE CARD

    /**
     * The server sends a request to any player who has a nope card. They then have the option to play it.
     */
    public static final String RequestNope = "RN";

    /**
     * The client then simply sends if the nope card should be used or not. True: means that the nope is used. False:
     * means that the nope is not used. In the event the targeted player also has a nope, they will also be sent the
     * RequestNope command.
     */
    public static final String RequestNopeResponse = "RNR";

    /**
     * Handles the above.
     */
    public static final String HandleRequestNope = "HRN";

//  FAVOR CARD
    /**
     * The server sends a request to the target player (of a favor card) and asks them to pick a card from their hand.
     */
    public static final String RequestFavor = "RF";

    /**
     * The client then responds with the index in their hand.
     */
    public static final String ChooseFavor = "CF";

    /**
     * Handles the above.
     */
    public static final String HandleRequestFavor = "HRF";
//  STARTING OR LEAVING A GAME
    public static final String StartGame = "SG";
    public static final String HandleStartGame = "HSG";
    public static final String LeaveGame = "LG";
    public static final String HandleLeaveGame = "HLG";

//  SELECTING A GAME
    public static final String RequestName = "RNA";
    public static final String SelectName = "SN";
    public static final String HandleSelectName = "HSN";

//  CHAT
    /**
     * User sends a message, Host player is used to indicate LOBBY. If a lobby is associated with a game that has
     * started, then the message is stored here.
     */
    public static final String SendMessage = "SM";

    /**
     * The message is then saved by the server and forwarder to each individual player still in the game.
     * Excludes exploded players.
     */
    public static final String ForwardMessage = "FM";

    /**
     * Does the above.
     */
    public static final String DoForwardMessage = "DFM";


//  ANNOUNCEMENTS
    /**
     * Server sends a message to all players currently in the game. A convenient way to simply have basic announcements
     * for all players.
     */
    public static final String AnnounceInGeneral = "AIG";

    /**
     * Does the above.
     */
    public static final String DoGeneralAnnounce = "DGA";

    /**
     * Sends a message to a specific player, based on the socket connection.
     */
    public static final String AnnounceInPrivate = "AIP";

    /**
     * Does the above.
     */
    public static final String DoPrivateAnnounce = "DPA";

    /**
     * Primarily to separate the errors from regular messages, so to allow the client to separately display said
     * messages. Usually this is followed by sending a new request again.
     *
     * Example:
     * // CLIENT -> PlayCard
     * // SERVER -> Invalid Action (e.g. skip but no player. This example would be a status code).
     * // SERVER -> RequestAction
     * //CLIENT -> PlayCard
     */
    public static final String DisplayErrorCode = "DEC";

    /**
     * Does the above.
     */
    public static final String DoErrorDisplay = "DED";

    /**
     * (Similar to DEC)
     * > (example) Code-Message
     */
    public static final String DisplayStatusCode = "DSC";

    /**
     * Does the above.
     */
    public static final String DoStatusDisplay = "DSD";
}
