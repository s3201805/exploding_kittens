
# Exploding Kittens

A brief description of what this project does and who it's for

This project performs a game of exploding kittens over a network and locally with two players, one of which can be a computer player.
## Authors

- [@Clara Prioteasa](https://www.github.com/claraprioteasa)

- [@Sophie Ligteringen](https://www.github.com/Ligteringen)
## Appendix

A complete game of exploding kittens with two people can be played locally by running the main method of the Exploding Kittens class. In here, the standard number of players is two. This means that when only one human player is involved, the other player will automatically be a computer player. This computer player will only perform valid moves. In the determineMove method, if the player has a ‘see the future’ card, this one will be played. Else, when the player has an attack card, this will be played. When obtained neither of these two, the Computer Player will just end their turn and draw a card. 

NETWORKING INCOMPLETE

The networking part is not complete. The application can link a Server and multiple Clients. This is done by running the main method in the Server class, followed by (2) Client classes. The next step is performing a handshake, the user is required to type in ‘MH;<name>;human_player’. Then the client will be linked to the Server. When the user wants the Client to be a computer player, replace ‘human_player’ with ‘computer_player’. The user is added as a player in the game. Then, when input ‘SG’, the game is started up. The problem is that due to a lack of time, this is where it stops. The game starts up on the Server, asking for input here. There is data shown after the start of the game to the client, however since there is a problem that somewhere the players are probably not correctly added to the game, the data has empty spots. What can also be noticed is that a majority of the methods in the ClientHandler and the ServerHandler are filled in. The Client and Server classes are complete, it is merely the actual initial connection to the game that stood in the way. 

CHAT FUNCTION

Although the game can technically not be played on the network,  a feature that can be found is the chat function, simply inpu ‘SM;<your name>;<message>’, the server will forward the message (mentioning from which user the message comes from) to every user connected to the server. This can also be seen in the user interface. 